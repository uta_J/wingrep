package com.john.main;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Grep {
    public static void main(String[] args){
        if(args.length < 2){
            System.out.println("You must have 2 more arguments to start WinGrep.\n" +
                    " Please use this command: java Grep 'regex or word' 'file to process'");

        }else{
            File file = null;
            try {
                file = new File(args[args.length-1]);
                if(!file.canRead())
                    return;
            }catch(Exception ex){
                System.out.println("Can't find your file. Please, try to use complete path to your file.");
                System.exit(0);
            }
            System.out.println("Welcome to WinGrep!");
            findAllRegEx(args, file);
            System.out.println("Thanks for using WinGrep!");
        }
    }

    private static void findAllRegEx(String[] regex, File file){
        System.out.println("Starting program...");
        Pattern pattern;
        Matcher matcher;
        ArrayList<String> strings = fileReader(file);
        if(strings != null) {
            for(int j = 0; j < regex.length-1; j++) {
                pattern = Pattern.compile(regex[j]);
                System.out.format("Finding substrings for %s ...\n", regex[j]);

                for (int i = 0; i < strings.size(); i++) {
                    matcher = pattern.matcher(strings.get(i));
                    if (matcher.find()) {
                        System.out.println("- " + strings.get(i));
                    }
                }
            }
        }else{
            System.out.println("Your file is empty.");
        }
    }

    private static ArrayList<String> fileReader(File file){
        System.out.println("Processing file...");
        try{
            FileInputStream fstream = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            ArrayList<String> strings = new ArrayList<>();
            int index = 0;

            String line = "";
            while((line = br.readLine()) != null){
                strings.add(index, line);
                index++;
            }
            if(strings.size()!=0)
                return strings;
            else return null;
        }catch(IOException ex){
            System.out.println("Error reading file");
        }
        return null;
    }
}
